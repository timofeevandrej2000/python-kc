def my_func(a):
    repeated = set()
    unique = []
    for i in a:
        if i not in repeated:
            unique.append(i)
            repeated.add(i)
            print(i)
    return tuple(reversed(unique))
a = [1, 2, 3, 6, 4, 3, 6, 5, 1, 2]
my_func(a)