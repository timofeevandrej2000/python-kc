class Car:
    def __init__(self, brand, model, year, equipment):
        self.brand = brand
        self.model = model
        self.year = year
        self.equipment = equipment
        
car = Car("Audi", "RS7", 2018, "luxe")
print(car.brand, car.model, car.year, car.equipment)