def func(a, b):
    d = list(set(a))
    c = set(b)
    for i in d:
        if i not in b:
            print(i, end = ' ')
    print()
a = ['a', 'b', 'c', 'd', 'e', 'f']
b = ['a', 'b', 'd']
func(a, b)