import random

random.seed(random.randint(1, 1000))
my_dict = {i: random.randint(1, 100) for i in range(1, 10)}
print(my_dict)
count = 0

while count != 3:
    max = 0
    for i in my_dict:
        if max < my_dict[i]:
            max = my_dict[i]
            j = i
    del my_dict[j]
    print(max)
    count += 1
