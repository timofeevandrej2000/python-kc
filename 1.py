count = 0

def decorat(func):
    def wrapper():
        func()
    return wrapper

@decorat
def func_count():
    global count
    count += 1

func_count()
func_count()

print(count)
